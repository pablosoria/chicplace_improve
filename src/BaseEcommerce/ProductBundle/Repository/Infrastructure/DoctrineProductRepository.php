<?php

namespace BaseEcommerceProductBundle\Repository\Infrastructure;

use BaseEcommerce\Bundles\Core\ProductBundle\Entity\Product;
use BaseEcommerceProductBundle\Repository\ProductRepository;
use Doctrine\ORM\EntityRepository;

class DoctrineProductRepository extends EntityRepository implements ProductRepository
{
    public function save(Product $product)
    {
        $this->_em->persist($product);
        $this->_em->flush();
    }

    public function activateShopProducts($shopId)
    {
        $query = $this->createQueryBuilder('p')
            ->update()
            ->set("p.enabled", true)
            ->where("p.enabled = :enabled")
            ->andWhere('p.deleted = :deleted')
            ->andWhere('p.shop = :shopId')
            ->setParameter('enabled', false)
            ->setParameter('deleted', false)
            ->setParameter('shopId', $shopId)
            ->getQuery();

        $query->execute();
    }

    public function getShopProducts($shopId, $translationLocale, $isArray = false)
    {
        $productsQuery = $this->createQueryBuilder('p')
            ->select('
                p.id, pt.name, p.family, im.id as principalImageId'
            )
            ->innerJoin('p.translations', 'pt')
            ->leftJoin('p.principalImage', 'im')
            ->where('p.shop = :shopId')
            ->andWhere('pt.locale = :locale')
            ->andWhere('p.enabled = :enabled')
            ->andWhere('p.deleted = :deleted')
            ->setParameter('locale', $translationLocale)
            ->setParameter('shopId', $shopId)
            ->setParameter('enabled', true)
            ->setParameter('deleted', false)
            ->getQuery();

        if ($isArray) {
            return $productsQuery->getArrayResult();
        }

        return $productsQuery->getResult();
    }

    public function getShopProductIds($shopId, $page = 1, $limit = 20)
    {
        $page = intval($page);
        if ($page < 1) {
            $page = 1;
        }

        $query = $this->createQueryBuilder('p')
            ->select('p.id')
            ->where("p.enabled = :enabled")
            ->andWhere('p.deleted = :deleted')
            ->andWhere('p.shop = :shopId')
            ->setParameter('enabled', true)
            ->setParameter('deleted', false)
            ->setParameter('shopId', $shopId)
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit)
            ->getQuery();

        return $query->getResult();
    }

    public function getShopItemsWithAttributes($shopId, $translationLocale)
    {
        $items = $this->createQueryBuilder('p')
            ->select('
                p.id productId, i.id as itemId, i.price, i.reducedPrice, i.stock'
            )
            ->innerJoin('p.items', 'i')
            ->where('p.shop = :shopId')
            ->andWhere('p.enabled = :enabled')
            ->andWhere('p.deleted = :deleted')
            ->andWhere('i.deleted = :deleted')
            ->setParameter('shopId', $shopId)
            ->setParameter('enabled', true)
            ->setParameter('deleted', false)
            ->getQuery()
            ->getArrayResult();

        return $items;
    }


}
