<?php

namespace BaseEcommerceProductBundle\Repository\Infrastructure;

use BaseEcommerce\Bundles\Core\ProductBundle\Entity\Item;
use BaseEcommerceProductBundle\Repository\ItemRepository;
use Doctrine\ORM\EntityRepository;

class DoctrineItemRepository extends EntityRepository implements ItemRepository
{
    public function save(Item $item)
    {
        $this->_em->persist($item);
        $this->_em->flush();
    }

    public function getItemsAttributes($itemsIds, $translationLocale)
    {
        $itemQueryBuilder = $this->createQueryBuilder('i');
        $itemsExtradata = $itemQueryBuilder
            ->select('i.id, '.$itemQueryBuilder->expr()->concat('vt.name', $itemQueryBuilder->expr()->concat($itemQueryBuilder->expr()->literal(' '), 'at.name')).' AS model')
            ->innerJoin('i.attributeValues', 'v')
            ->innerJoin('v.attribute', 'a')
            ->innerJoin('v.translations', 'vt')
            ->innerJoin('a.translations', 'at')
            ->where('i.id IN (:itemsIds)')
            ->andWhere('vt.locale = :locale')
            ->andWhere('at.locale = :locale')
            ->setParameter('itemsIds', $itemsIds)
            ->setParameter('locale', $translationLocale)
            ->getQuery()
            ->getArrayResult();

        return $itemsExtradata;
    }

    public function runPriceQuery($field = 'price', $productId)
    {
        if (!is_array($productId)) {
            $productId = array($productId);
        }

        $query = $this->createQueryBuilder('i')
            ->select('MIN(i.'.$field.') as price, IDENTITY(i.product) as id')
            ->where("i.enabled = :enabled")
            ->andWhere('i.deleted = :deleted')
            ->andWhere('i.product IN (:product)')
            ->andWhere('i.price > 0')
            ->setParameter('enabled', true)
            ->setParameter('deleted', false)
            ->setParameter('product', $productId)
            ->addGroupBy('i.product');

        return $query->getQuery();
    }


}
