<?php

namespace BaseEcommerceProductBundle\Repository;

use BaseEcommerce\Bundles\Core\ProductBundle\Entity\Item;

interface ItemRepository
{
    public function save(Item $product);
    public function getItemsAttributes($itemsIds, $translationLocale);
    public function runPriceQuery($field = 'price', $productId);

}
