<?php

namespace BaseEcommerceProductBundle\Repository;

use BaseEcommerce\Bundles\Core\ProductBundle\Entity\Product;

interface ProductRepository
{
    public function save(Product $product);
    public function activateShopProducts($shopId);
    public function getShopProducts($shopId, $translationLocale, $isArray = false);
    public function getShopItemsWithAttributes($shopId, $translationLocale);

}
