<?php

namespace Chicplace\ProductBundle\Repository;

use Chicplace\ProductBundle\Entity\ProductExtraData;

interface ProductExtraDataRepository
{
    public function save(ProductExtraData $productExtraData);
}
