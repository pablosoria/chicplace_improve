<?php

namespace BaseEcommerceProductBundle\Repository\Infrastructure;

use Chicplace\ProductBundle\Entity\ProductExtraData;
use Chicplace\ProductBundle\Repository\ProductExtraDataRepository;
use Doctrine\ORM\EntityRepository;

class DoctrineProductExtraDataRepository extends EntityRepository implements ProductExtraDataRepository
{
    public function save(ProductExtraData $productExtraData)
    {
        $this->_em->persist($productExtraData);
        $this->_em->flush();
    }
}
