<?php

namespace Chicplace\CoreBundle\Services;

use BaseEcommerce\Bundles\Core\ProductBundle\Entity\Product;
use BaseEcommerce\Bundles\Core\ProductBundle\Entity\ProductTranslation;
use BaseEcommerceProductBundle\Repository\ItemRepository;
use BaseEcommerceProductBundle\Repository\ProductRepository;
use Chicplace\CouponBundle\Repository\DiscountRepository;
use Chicplace\ProductBundle\Entity\ProductExtraData;
use Chicplace\ProductBundle\Repository\ProductExtraDataRepository;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Snc\RedisBundle\Client\Phpredis\Client;
use Redis;

/**
 * Product manager service
 */
class ProductManager extends \BaseEcommerce\Bundles\Core\ProductBundle\Services\ProductManager
{

    /**
     * @var ProductTranslationManager
     */
    private $productTranslationManager;

    /**
     * @var Redis Client
     */
    private $redis;

    /**
     * @var ScoreListenerInterface
     */
    private $scoreListener;

    CONST QUEUE_NAME = "product_sync";


    /**
     * @param ProductRepository             $productRepository
     * @param ItemRepository                $itemRepository
     * @param DiscountRepository            $discountRepository
     * @param ProductExtraDataRepository    $productExtraDataRepository
     * @param Redis|Client                  $redis                     Redis client
     * @param ScoreListenerInterface        $scoreListener             ScoreListener Interface
     * @param ProductTranslationManager     $productTranslationManager Product Translation Manager
     */
    public function __construct(ProductRepository $productRepository,
                                ItemRepository $itemRepository,
                                DiscountRepository $discountRepository,
                                ProductExtraDataRepository $productExtraDataRepository,
                                $redis,
                                $scoreListener,
                                ProductTranslationManager $productTranslationManager)
    {
        $this->productRepository = $productRepository;
        $this->itemRepository = $itemRepository;
        $this->discountRepository = $discountRepository;
        $this->productExtraDataRepository = $productExtraDataRepository;
        $this->redis = $redis;
        $this->scoreListener = $scoreListener;
        $this->productTranslationManager = $productTranslationManager;
    }

    /**
     * Event so we can calculate product score
     *
     * @param LifecycleEventArgs $args
     */
    public function calculateProductScore(LifecycleEventArgs $args)
    {
        $product = $this->checkProduct($args->getEntity());
        if (is_null($product)) {
            return;
        }

        $product->setScore($this->scoreListener->calculateScore($product));
        if ($args instanceof PreUpdateEventArgs) {
            $uow = $args->getEntityManager()->getUnitOfWork();
            $uow->recomputeSingleEntityChangeSet(
                $args->getEntityManager()->getClassMetadata(get_class($product)),
                $product
            );
        }
    }

    /**
     * Checks if $entity is a product
     *
     * @param object $entity
     *
     * @return Product|null
     */
    private function checkProduct($entity)
    {
        $product = null;
        if ($entity instanceof Product) {
            $product = $entity;
        } elseif ($entity instanceof ProductTranslation) {
            $product = $entity->getTranslatable();
        }

        return $product;
    }

    /**
     * This action should be performed on postPersist and postUpdate
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function processProduct(LifecycleEventArgs $eventArgs)
    {
        $product = $this->checkProduct($eventArgs->getEntity());
        if (is_null($product)) {
            return;
        }

        $productsToUpdate = json_decode($this->redis->get(self::QUEUE_NAME), true);
        if ($productsToUpdate === null) {
            $productsToUpdate = array();
        }

        $productsToUpdate[] = $product->getId();

        $this->redis->set(self::QUEUE_NAME, json_encode(array_unique($productsToUpdate)));
    }

    /**
     * Populate changes to related entities to product
     */
    public function populateChangesToRelated()
    {
        $productsToUpdate = json_decode($this->redis->get(self::QUEUE_NAME), true);
        //check that there is something to update
        if ($productsToUpdate === null || count($productsToUpdate) == 0) {
            return;
        }

        foreach ($productsToUpdate as $productId) {
            $product = $this->productRepository->find($productId);
            if ($product instanceof Product) {
                $this->productTranslationManager->doTranslation($product);
                $this->removeProductFromCacheQueue($product->getId());
            }
        }
    }

    /**
     * Remove product's id redis cache queue
     *
     * @param $productId
     */
    private function removeProductFromCacheQueue($productId)
    {
        $productsToUpdate = json_decode($this->redis->get(self::QUEUE_NAME), true);
        if ($productsToUpdate === null) {
            $productsToUpdate = array();
        }
        $this->redis->set(self::QUEUE_NAME, json_encode(array_diff($productsToUpdate, array($productId))));
    }

    /**
     * If product doesn't have any customizable field it set to not customizable
     * @param $productId
     */
    public function removeIsCustomizable($productId)
    {
        $product = $this->productRepository->find($productId);

        if ($product instanceof Product && count($product->getCustomizableFields()) < 1) {
            $product->setCustomizable(false);
            $this->productRepository->save($product);
        }
    }

    /**
     * Translate shop's products
     *
     * @param array $products
     */
    public function translateShopProducts($products)
    {
        foreach ($products as $product) {
            $this->productTranslationManager->doTranslation($product);
        }
    }

    /**
     * Activate shop products
     *
     * @param $shopId
     */
    public function activateShopProducts($shopId)
    {
        $this->productRepository->activateShopProducts($shopId);
    }

    /**
     * Get shop products data for email
     *
     * @param int    $shopId
     * @param string $translationLocale
     */

    /**
     * @param int    $shopId
     * @param string $translationLocale
     * @param bool   $isArray
     * @return mixed List of products. Hydrated like array or objects.
     */
    public function getShopProducts($shopId, $translationLocale, $isArray = false)
    {
        return $this->productRepository->getShopProducts($shopId, $translationLocale, $isArray);
    }

    /**
     * Get all active products id's from a shop
     *
     * @param int $shopId
     * @param int $page   default 1  page to get
     * @param int $limit  default 20 how many entries to get
     *
     * @return array with the results in the form of array(0 => array('id' => x)...)
     */
    public function getShopProductIds($shopId, $page = 1, $limit = 20)
    {

        return $this->productRepository->getShopProductIds($shopId, $page, $limit);

    }

    /**
     * Get items and their attributes and values of specific shop
     *
     * @param int    $shopId
     * @param string $translationLocale
     *
     * @return array
     */
    public function getShopItemsWithAttributes($shopId, $translationLocale)
    {

        $items = $this->productRepository->getShopItemsWithAttributes($shopId, $translationLocale);

        foreach ($items as $item) {
            $itemsIds[] = $item['itemId'];
            $itemsCollection[$item['itemId']] = $item;
        }

        $itemsExtradata = $this->getItemsAttributes($itemsIds, $translationLocale);

        foreach ($itemsExtradata as $itemExtradata) {
            $itemsCollection[$itemExtradata['id']]['models'][] = $itemExtradata['model'];
        }

        return $itemsCollection;
    }

    /**
     * Get attributes and values of items
     *
     * @param array   $itemsIds
     * @param string $translationLocale
     *
     * @return mixed
     */
    protected function getItemsAttributes($itemsIds, $translationLocale)
    {
        return $this->itemRepository->getItemsAttributes($itemsIds, $translationLocale);
    }

    /**
     * @param $productId
     *
     * @return mixed
     */
    public function getPrice($productId)
    {
        return $this->formatReturnPrices($this->runPriceQuery('price', $productId)->getResult());
    }

    /**
     * @param $productId
     *
     * @return mixed
     */
    public function getReducedprice($productId)
    {
        return $this->formatReturnPrices($this->runPriceQuery('reducedPrice', $productId)->getResult());
    }


    /**
     * @param $productId
     * @return mixed
     */
    public function getDiscounts($productId)
    {
        $results = $this->discountRepository->getDiscounts($productId);
        $finalResults = array();

        foreach ($results as $result) {
            $finalResults[$result['id']] = array('type' => $result['type'], 'value' => $result['value']);
        }

        return $finalResults;
    }


    /**
     * @param $results
     *
     * @return array
     */
    private function formatReturnPrices($results)
    {
        $finalResults = array();
        foreach ($results as $result) {
            $finalResults[$result['id']] = $result['price'];
        }

        return $finalResults;
    }

    /**
     * @param string $field
     * @param $productId
     * @return mixed
     */
    private function runPriceQuery($field = 'price', $productId)
    {
        return $this->itemRepository->runPriceQuery($field, $productId);
    }

    /**
     * Set if product is penalized
     *
     * @param Product | int $product
     * @param bool          $penalized
     */
    public function setProductPenalize($product, $penalized)
    {
        $productId = ($product instanceof Product)? $product->getId() : $product;
        $productExtraData = $this->productExtraDataRepository->findOneByProduct($productId);

        if (!($productExtraData instanceof ProductExtraData)) {
            $productExtraData = new ProductExtraData();
            $product = ($product instanceof Product) ? $product : $this->productRepository->find($productId);
            $productExtraData->setProduct($product);
        }

        $productExtraData->setPenalized($penalized);
        $this->productExtraDataRepository->save($productExtraData);
    }
}
