<?php

namespace Chicplace\CouponBundle\Repository;

use Chicplace\CouponBundle\Entity\Discount;

interface DiscountRepository
{
    public function save(Discount $discount);
    public function getDiscounts($productId);
}
