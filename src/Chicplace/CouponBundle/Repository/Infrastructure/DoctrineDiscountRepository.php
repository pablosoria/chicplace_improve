<?php

namespace BaseEcommerceProductBundle\Repository\Infrastructure;

use Chicplace\CouponBundle\Entity\Discount;
use Chicplace\CouponBundle\Repository\DiscountRepository;
use Doctrine\ORM\EntityRepository;

class DoctrineDiscountRepository extends EntityRepository implements DiscountRepository
{
    public function save(Discount $discount)
    {
        $this->_em->persist($discount);
        $this->_em->flush();
    }

    public function getDiscounts($productId)
    {
        if (!is_array($productId)) {
            $productId = array($productId);
        }

        $query = $this->createQueryBuilder('d')
            ->select('d.value, d.type, IDENTITY(pd.product) as id')
            ->innerJoin('d.productDiscounts', 'pd')
            ->where('pd.product IN (:productId)')
            ->andWhere('d.validFrom <= :now')
            ->andWhere('d.validTo >= :now')
            ->andWhere('d.enabled = :enabled')
            ->andWhere('d.deleted = :deleted')
            ->setParameter('productId', $productId)
            ->setParameter('now', date('Y-m-d'))
            ->setParameter('enabled', true)
            ->setParameter('deleted', false)
            ->addGroupBy('pd.product');

        return $query->getQuery()->getResult();
    }

}
